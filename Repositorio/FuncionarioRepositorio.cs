﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Repositorio
{
    public static class FuncionarioRepositorio
    {
        public static List<Funcionario> ObterListaFuncionario()
        {
            List<Funcionario> FuncionarioLista = new List<Funcionario>();

            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Funcionario.ObterListaFuncionarios;

                SqlCommand cmd = new SqlCommand(sql, conection);

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        FuncionarioLista.Add(new Funcionario
                        {
                            IdFuncionario = Convert.ToInt32(rd["Id"]),
                            IdPessoaFisica = Convert.ToInt32(rd["IdPessoaFisica"]),
                            IdFuncao = Convert.ToInt32(rd["IdFuncao"]),
                            Salario = Convert.ToDecimal(rd["Salario"]),
                            DataAdmissao = Convert.ToDateTime(rd["DataAdminissao"]),
                            Ativo = Convert.ToBoolean(rd["Ativo"])
                        });
                    }
                }
                return FuncionarioLista;

            }
        }

        public static void InserirFuncionario(Funcionario Funcionario)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.Funcionario.InserirFuncionario;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue("@IdPessoaFisica", Funcionario.IdPessoaFisica);
                    cmd.Parameters.AddWithValue("@IdFuncao", Funcionario.IdFuncao);
                    cmd.Parameters.AddWithValue("@Salario", Funcionario.Salario);
                    cmd.Parameters.AddWithValue("@DataAdmissao", Funcionario.DataAdmissao);
                    cmd.Parameters.AddWithValue("@DataDemissao", Funcionario.DataAdmissao);
                    cmd.Parameters.AddWithValue("@Ativo", Funcionario.Ativo);

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void AtualizaFuncionario(Funcionario Funcionario)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.Funcionario.AtualizarFuncionario;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue(@"IdFuncionario", Funcionario.IdFuncionario);

                    cmd.Parameters.AddWithValue("@IdPessoaFisica", Funcionario.IdPessoaFisica);
                    cmd.Parameters.AddWithValue("@IdFuncao", Funcionario.IdFuncao);
                    cmd.Parameters.AddWithValue("@Salario", Funcionario.Salario);
                    cmd.Parameters.AddWithValue("@DataAdmissao", Funcionario.DataAdmissao);
                    cmd.Parameters.AddWithValue("@DataDemissao", Funcionario.DataAdmissao);
                    cmd.Parameters.AddWithValue("@Ativo", Funcionario.Ativo);

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void DeletarFuncionario(int id)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Repositorio.Scripts.Funcionario.DeletarFuncionario;
                    SqlCommand cmd = new SqlCommand(sql, conection);
                    cmd.Parameters.AddWithValue(@"IdFuncionario", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public static Funcionario ObterFuncionarioPorId(int id)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Funcionario.ObterFuncionarioPorId;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IdFuncionario", Value = id });
                Funcionario Funcionario = null;
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    Funcionario = new Funcionario
                    {
                        IdFuncionario = Convert.ToInt32(rd["Id"]),
                        IdPessoaFisica = Convert.ToInt32(rd["IdPessoaFisica"]),
                        IdFuncao = Convert.ToInt32(rd["IdFuncao"]),
                        Salario = Convert.ToDecimal(rd["Salario"]),
                        DataAdmissao = Convert.ToDateTime(rd["DataAdminissao"]),
                        Ativo = Convert.ToBoolean(rd["Ativo"])
                    };
                }

                return Funcionario;
            }
        }

        public static List<Funcao> ObterListaFuncao()
        {
            List<Funcao> funcaoLista = new List<Funcao>();

            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Funcionario.ObterListaFuncao;

                SqlCommand cmd = new SqlCommand(sql, conection);

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        funcaoLista.Add(new Funcao
                        {
                            IdFuncao = Convert.ToInt32(rd["Id"]),
                            Descricao = Convert.ToString(rd["Nome"])
                        });
                    }
                }
                return funcaoLista;
            }
        }
    }
}
