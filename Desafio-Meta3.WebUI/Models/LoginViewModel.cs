﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desafio_Meta3.WebUI.Models
{
    public class LoginViewModel
    {
        public Usuario Usuario { get; set; }
        public bool LembrarSenha { get; set; }
    }
}